import requests
import logging
import time
from requests.exceptions import RequestException


logger = logging.getLogger(__name__)


def get_course_list(api_key):
    """
    Queries a list of courses from ANS.
    Currently uses a hardcoded value for the school, which should be changed if the app is ever extended to also be used outside of TU Delft.
    
    Args:
        api_key (str): The API key for authentication.
    
    Returns:
        list: A list of courses that the user can access in ANS using their API key.
    
    Raises:
        requests.exceptions.RequestException: If the request fails.
        requests.exceptions.HTTPError: If the response contains an unsuccessful status code.
    """
    header = {"accept": "application/json", "Authorization": f"Bearer {api_key}"}
    try:
        response = requests.get("https://ans.app/api/v2/schools/1/courses?limit=100&page=1", headers=header, timeout=(10, 30))
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        logger.error(f"Request failed: {e}")
        raise

# def get_course_list(api_key):
#     """
#     Queries a list of courses from ANS, stopping immediately when duplicates are detected.
#     """
#     header = {"accept": "application/json", "Authorization": f"Bearer {api_key}"}
#     all_courses = []
#     seen_course_ids = set()
#     page = 1
#     limit = 100
#     base_delay = 1

#     try:
#         while True:
#             time.sleep(base_delay)
            
#             url = f"https://ans.app/api/v2/schools/1/courses?limit={limit}&page={page}"
#             response = requests.get(url, headers=header, timeout=(10, 30))
#             response.raise_for_status()
            
#             courses = response.json()
#             if not courses:
#                 logger.info("No more courses found")
#                 break

#             # Check first course for duplicate
#             if courses and courses[0]['id'] in seen_course_ids:
#                 logger.info(f"Stopping at page {page} - detected duplicate courses")
#                 break

#             # Process new courses
#             for course in courses:
#                 course_id = course['id']
#                 # Check if course year is 2022/2023 or later
#                 course_year = course.get('year', '')
#                 if not course_year or course_year < '2022/2023':
#                     continue
                
#                 if course_id not in seen_course_ids:
#                     seen_course_ids.add(course_id)
#                     all_courses.append(course)

#             logger.info(f"Successfully fetched page {page}, total unique courses: {len(all_courses)}")
#             page += 1

#     except RequestException as e:
#         logger.error(f"Request failed: {e}")
#         raise

#     return sorted(all_courses, key=lambda course: course['course_code'])
    