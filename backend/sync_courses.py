import requests
import logging
from datetime import datetime
from django.utils import timezone
from django.db import transaction
from .models import Course, UserCourse

logger = logging.getLogger(__name__)

def sync_courses(api_key: str) -> None:
    """
    Syncs all available courses from ANS to our database.
    """
    headers = {
        "accept": "application/json",
        "Authorization": f"Bearer {api_key}"
    }
    
    page = 1
    limit = 100
    
    try:
        while True:
            logger.info(f"Fetching courses page {page}")
            response = requests.get(
                "https://ans.app/api/v2/search/courses",
                params={
                    "limit": limit,
                    "page": page
                },
                headers=headers,
                timeout=(10, 30)
            )
            response.raise_for_status()
            
            courses = response.json()
            if not courses:  # No more courses
                break
            
            logger.info(f"Found {len(courses)} courses on page {page}")
                
            # Batch update courses
            with transaction.atomic():
                for course_data in courses:
                    Course.objects.update_or_create(
                        ans_id=course_data['id'],
                        defaults={
                            'name': course_data['name'],
                            'course_code': course_data.get('course_code'),
                            'year': course_data.get('year', timezone.now().year),
                            'created_at': datetime.fromisoformat(course_data['created_at'].replace('Z', '+00:00')),
                            'updated_at': datetime.fromisoformat(course_data['updated_at'].replace('Z', '+00:00')),
                            'self_enroll': course_data.get('self_enroll', False),
                            'trashed': course_data.get('trashed', False),
                            'trashed_at': datetime.fromisoformat(course_data['trashed_at'].replace('Z', '+00:00')) if course_data.get('trashed_at') else None,
                            'school_id': course_data.get('school_id')
                        }
                    )
            
            if len(courses) < limit:
                break
                
            page += 1
            
    except Exception as e:
        logger.error(f"Failed to sync courses: {str(e)}")
        raise

def sync_user_courses(user_id: str, courses_data: list) -> None:
    """
    Syncs course access for a specific user.
    """
    try:
        logger.info(f"Syncing courses for user {user_id}")
        with transaction.atomic():
            # First, remove all existing user course relationships
            UserCourse.objects.filter(user_id=user_id).delete()
            
            # Then create new relationships
            for course_data in courses_data:
                course = Course.objects.filter(ans_id=course_data['id']).first()
                if course:
                    UserCourse.objects.create(
                        user_id=user_id,
                        course=course,
                        role=course_data.get('role', ''),
                        role_id=course_data.get('role_id')
                    )
                    
            logger.info(f"Successfully synced {len(courses_data)} courses for user {user_id}")
    except Exception as e:
        logger.error(f"Failed to sync user courses: {str(e)}")
        raise