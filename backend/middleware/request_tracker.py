from django.core.cache import cache
from datetime import datetime
import json
import logging

logger = logging.getLogger('api.requests')

class RequestTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Skip admin and static requests
        if not request.path.startswith('/api/'):
            return self.get_response(request)

        # Track request
        self._track_request(request)
        
        response = self.get_response(request)
        return response

    def _track_request(self, request):
        today = datetime.now().strftime('%Y-%m-%d')
        endpoint = request.path
        
        # Get current counts from cache
        stats_key = f'api_stats_{today}'
        stats = cache.get(stats_key) or {}
        
        # Update counts
        if endpoint not in stats:
            stats[endpoint] = 0
        stats[endpoint] += 1
        
        # Save to cache
        cache.set(stats_key, stats, 60*60*24)  # Store for 24 hours 

        logger.info(f"API Request: {request.method} {request.path}")