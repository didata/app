import requests
import logging

logger = logging.getLogger(__name__)

def verify_api_key(key):
    header = {"accept": "application/json", "Authorization": f"Bearer {key}"}
    try:
        response = requests.get(f"https://ans.app/api/v2/schools/1/courses", headers=header, timeout=(10, 30))
        # Return the actual response instead of the response object
        return response.status_code == 200
    except requests.exceptions.RequestException as e:
        logger.error(f"API key verification failed: {str(e)}")
        return False