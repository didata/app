from collections import defaultdict, Counter
import numpy as np
import json
from django.core.serializers.json import DjangoJSONEncoder
import logging

logger = logging.getLogger(__name__)

class NumpyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NumpyEncoder, self).default(obj)

# def analyse_by_question(questions_raw):
#     logger.info("Starting analyse_by_question")
#     res = {}
#     for question_key, question_data in questions_raw.items():
#         scores = question_data['scores']
#         attempted = question_data['attempted']
#         checked = question_data['checked']
        
#         aggr_data = {
#             '# Fully correct': sum(1 for score in scores if score == 1.0),
#             '# Fully incorrect': sum(1 for score in scores if score == 0.0),
#             '# Partially correct': sum(1 for score in scores if score is not None and 0 < score < 1),
#             '# Not attempted': sum(1 for attempt in attempted if not attempt),
#             '# Not checked': sum(1 for check in checked if not check),
#             'Partial marks': defaultdict(int),
#             'Total submissions': len(scores)
#         }
        
#         # Calculate partial marks
#         for score in scores:
#             if score is not None and 0 < score < 1:
#                 aggr_data['Partial marks'][score] += 1
        
#         # Convert defaultdict to regular dict for JSON serialization
#         aggr_data['Partial marks'] = dict(aggr_data['Partial marks'])
        
#         res[question_key] = aggr_data
    
#     logger.info(f"Results from analyse_by_question: {res}")
#     return res

def analyse_by_question(questions_raw):
    res = {}

    for question_key, question_data in questions_raw.items():
        scores = question_data['scores']
        attempted = question_data['attempted']
        checked = question_data['checked']
        aggr_data = {}

        aggr_data['# Not checked'] = checked.count(False)
        aggr_data['# Not attempted'] = attempted.count(False)
        aggr_data['# Fully incorrect'] = sum([score == 0 and attempt == True for (score, attempt) in zip(scores, attempted)])
        aggr_data['# Partially correct'] = sum(0 < score < 1 for score in scores)
        aggr_data['# Fully correct'] = scores.count(1)
        aggr_data['Partial marks'] = dict(Counter([score for score in scores if 0 < score < 1]))

        res[question_key] = aggr_data
    
    return res


def analyse_by_exercise(questions_raw):
    print("Starting analyse_by_exercise")
    aggr_results = defaultdict(lambda: {
        '# Not checked': 0, 
        '# Not attempted': 0,
        'Student Scores': defaultdict(lambda: {'total_score': 0, 'attempts': 0})
    })

    for question_id, question_data in questions_raw.items():
        exercise_number = ''.join(filter(str.isdigit, question_id.split('.')[0]))
        print(f"Processing question {question_id} for exercise {exercise_number}")
        
        aggr_results[exercise_number]['# Not checked'] += question_data['# Not checked']
        aggr_results[exercise_number]['# Not attempted'] += question_data['# Not attempted']

        total_students = question_data['# Fully correct'] + question_data['# Fully incorrect'] + question_data['# Partially correct']
        for i in range(total_students):
            if i < question_data['# Fully correct']:
                score = 1
            elif i < question_data['# Fully correct'] + question_data['# Fully incorrect']:
                score = 0
            else:
                score = next(iter(question_data['Partial marks']), 0.5)  # Fallback to 0.5 if no partial marks

            aggr_results[exercise_number]['Student Scores'][i]['total_score'] += score
            aggr_results[exercise_number]['Student Scores'][i]['attempts'] += 1

    res = defaultdict(lambda: {
        '# Not checked': 0, 
        '# Not attempted': 0, 
        '# Fully correct': 0,
        '# Fully incorrect': 0,
        '# Partially correct': 0,
        'Partial marks': {}
    })

    for exercise, data in aggr_results.items():
        partial_marks = []
        for student_id, scores_data in data['Student Scores'].items():
            if scores_data['attempts'] > 0:
                avg = scores_data['total_score'] / scores_data['attempts']
                if avg == 1:
                    res[exercise]["# Fully correct"] += 1
                elif avg == 0:
                    res[exercise]["# Fully incorrect"] += 1
                else:
                    partial_marks.append(avg)
                    res[exercise]["# Partially correct"] += 1
        res[exercise]["# Not checked"] = data["# Not checked"]
        res[exercise]["# Not attempted"] = data['# Not attempted']
        res[exercise]["Partial marks"] = dict(Counter(partial_marks))
        
        print(f"Results for exercise {exercise}: {res[exercise]}")

    print(f"Final exercise results: {dict(res)}")
    return dict(res)