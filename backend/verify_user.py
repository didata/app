import requests
import logging
from rest_framework.response import Response

logger = logging.getLogger(__name__)

def get_user_by_email(email, api_key):
    """
    Search for a user by email in ANS.
    
    Args:
        email (str): User's email address
        api_key (str): The API key for authentication
    
    Returns:
        dict: User data if found, None otherwise
    """
    header = {"accept": "application/json", "Authorization": f"Bearer {api_key}"}
    try:
        # Added limit and page parameters to match your working curl example
        response = requests.get(
            f"https://ans.app/api/v2/search/users",
            params={"query": f'email:"{email}"'},
            headers=header,
            timeout=(10, 30)
        )
        response.raise_for_status()
        users = response.json()
        logger.info(f"API Response: {users}")  # Add this to see the full response
        return users[0] if users else None
    except requests.exceptions.RequestException as e:
        logger.error(f"User search failed: {e}")
        raise

def get_user_courses(user_id, api_key):
    """
    Get courses where the user is an instructor or operator.
    
    Args:
        user_id (str): ANS user ID
        api_key (str): The API key for authentication
    """
    header = {"accept": "application/json", "Authorization": f"Bearer {api_key}"}
    try:
        response = requests.get(
            f"https://ans.app/api/v2/users/{user_id}",
            headers=header,
            timeout=(10, 30)
        )
        response.raise_for_status()
        user_data = response.json()
        
        # Filter courses where user is an instructor or operator
        authorized_courses = [
            course for course in user_data.get('courses', [])
            if course.get('role_id') == 1135 or course.get('role') in ['instructor', 'operator', 'reviewer']
        ]
        return authorized_courses
    except requests.exceptions.RequestException as e:
        logger.error(f"Failed to get user courses: {e}")
        raise

def get_user_by_id(user_id, api_key):
    """
    Get user details by ID from ANS API.
    """
    header = {"accept": "application/json", "Authorization": f"Bearer {api_key}"}
    try:
        response = requests.get(
            f"https://ans.app/api/v2/users/{user_id}",
            headers=header,
            timeout=(10, 30)
        )
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        logger.error(f"Failed to get user by ID: {e}")
        raise

def get_department_courses(department_id, api_key):
    """
    Get courses for a department from ANS API.
    """
    header = {"accept": "application/json", "Authorization": f"Bearer {api_key}"}
    try:
        response = requests.get(
            f"https://ans.app/api/v2/departments/{department_id}/courses",
            headers=header,
            timeout=(10, 30)
        )
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        logger.error(f"Failed to get department courses: {e}")
        raise