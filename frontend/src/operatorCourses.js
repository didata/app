import { API_URL } from './config';

export const getOperatorCourses = async (apiKey) => {
    const headers = {
        'Authorization': `Bearer ${apiKey}`,
        'Content-Type': 'application/json'
    };

    try {
        // First get all courses from the school
        const coursesResponse = await fetch(`${API_URL}/schools/1/courses`, {
            headers: headers
        });
        
        if (!coursesResponse.ok) {
            throw new Error('Failed to fetch courses');
        }

        const courses = await coursesResponse.json();

        // Filter courses based on current academic year and non-trashed status
        const currentYear = new Date().getMonth() < 8 ? 
            new Date().getFullYear() - 1 : 
            new Date().getFullYear();

        const filteredCourses = courses.filter(course => {
            return !course.trashed && course.year === currentYear;
        });

        // Sort courses by name
        const sortedCourses = filteredCourses.sort((a, b) => {
            const nameA = (a.course_code ? `${a.course_code} ` : '') + a.name;
            const nameB = (b.course_code ? `${b.course_code} ` : '') + b.name;
            return nameA.localeCompare(nameB);
        });

        return {
            courses: sortedCourses,
            total_count: sortedCourses.length
        };

    } catch (error) {
        console.error('Error fetching operator courses:', error);
        throw error;
    }
};

// Returns all courses for the current academic year
export const getCurrentAcademicYear = () => {
    const now = new Date();
    // Academic year starts in September
    return now.getMonth() < 8 ? now.getFullYear() - 1 : now.getFullYear();
};

// Helper function to get courses for a specific year
export const getOperatorCoursesByYear = async (apiKey, year) => {
    const headers = {
        'Authorization': `Bearer ${apiKey}`,
        'Content-Type': 'application/json'
    };

    try {
        const coursesResponse = await fetch(`${API_URL}/schools/1/courses`, {
            headers: headers
        });
        
        if (!coursesResponse.ok) {
            throw new Error('Failed to fetch courses');
        }

        const courses = await coursesResponse.json();

        // Filter courses based on specified year and non-trashed status
        const filteredCourses = courses.filter(course => {
            return !course.trashed && course.year === year;
        });

        // Sort courses by name
        const sortedCourses = filteredCourses.sort((a, b) => {
            const nameA = (a.course_code ? `${a.course_code} ` : '') + a.name;
            const nameB = (b.course_code ? `${b.course_code} ` : '') + b.name;
            return nameA.localeCompare(nameB);
        });

        return {
            courses: sortedCourses,
            total_count: sortedCourses.length,
            year: year
        };

    } catch (error) {
        console.error('Error fetching operator courses:', error);
        throw error;
    }
};