import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

export const useSessionHandler = () => {
  const navigate = useNavigate();

  const checkSession = async () => {
    const sessionToken = localStorage.getItem('sessionToken');
    
    if (!sessionToken) {
      handleLogout();
      return false;
    }

    try {
      const response = await fetch('/api/verify-session/', {
        headers: {
          'X-Session-Token': sessionToken,
          'Content-Type': 'application/json'
        }
      });

      if (!response.ok) {
        handleLogout();
        return false;
      }

      return true;
    } catch (error) {
      console.error('Session verification failed:', error);
      handleLogout();
      return false;
    }
  };

  const handleLogout = () => {
    // Clear all localStorage items
    localStorage.removeItem('sessionToken');
    localStorage.removeItem('userData');
    localStorage.removeItem('courses');
    localStorage.removeItem('apiKey');
    localStorage.removeItem('demoMode');

    // Redirect to login page
    navigate('/login');
  };

  // Set up interval to check session periodically
  useEffect(() => {
    const sessionCheck = setInterval(checkSession, 5 * 60 * 1000); // Check every 5 minutes
    
    // Initial check
    checkSession();

    return () => clearInterval(sessionCheck);
  }, [navigate]);

  return { checkSession, handleLogout };
};

export default useSessionHandler;