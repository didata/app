import { useState, useEffect } from 'react';

/**
 * Custom hook that delays the update of a value by a specified amount of time
 * to prevent too many rapid changes triggering API calls
 * 
 * @param {any} value - The value to debounce
 * @param {number} delay - The delay in milliseconds
 * @returns {any} - The debounced value
 */
export function useDebounce(value, delay = 500) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    // Set a timeout to update the debounced value after the specified delay
    const timer = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    // Clean up the timeout if the value changes before the delay has passed
    return () => {
      clearTimeout(timer);
    };
  }, [value, delay]);

  return debouncedValue;
}

/**
 * Track API request timestamps to prevent requests within a cooldown period
 */
const apiRequestTimestamps = new Map();
const API_COOLDOWN_MS = 2000; // Minimum time between identical requests

export function canMakeRequest(endpoint) {
  const now = Date.now();
  const lastRequest = apiRequestTimestamps.get(endpoint) || 0;
  
  if (now - lastRequest < API_COOLDOWN_MS) {
    return false;
  }
  
  apiRequestTimestamps.set(endpoint, now);
  return true;
}