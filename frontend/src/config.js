const API_URL = process.env.REACT_APP_API_URL || 'https://didata.tudelft.nl/api';
console.log('API_URL in config.js:', API_URL);
export { API_URL };