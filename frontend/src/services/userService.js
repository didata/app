import { API_URL } from '../config';

export const getUserRole = () => {
    const userData = JSON.parse(localStorage.getItem('userData') || '{}');
    return userData.role || null;
};

export const isOperator = () => {
    return getUserRole() === 'operator';
};

export const getSessionToken = () => {
    return localStorage.getItem('sessionToken');
};

export const getUserCourses = async (year = null) => {
    const sessionToken = getSessionToken();
    if (!sessionToken) return { courses: [], total_count: 0 };

    try {
        const url = year ? 
            `${API_URL}/courses/year/${year}/` : 
            `${API_URL}/courses/`;

        const response = await fetch(url, {
            headers: {
                'X-Session-Token': sessionToken,
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Failed to fetch courses');
        }

        const data = await response.json();
        return {
            courses: data.courses || [],
            total_count: data.total_count || 0
        };
    } catch (error) {
        console.error('Error fetching user courses:', error);
        throw error;
    }
};

export const getAvailableYears = async () => {
    const sessionToken = getSessionToken();
    if (!sessionToken) return { years: [], current_year: null };

    try {
        const response = await fetch(`${API_URL}/courses/years/`, {
            headers: {
                'X-Session-Token': sessionToken,
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Failed to fetch available years');
        }

        const data = await response.json();
        return {
            years: data.years || [],
            current_year: data.current_year
        };
    } catch (error) {
        console.error('Error fetching available years:', error);
        throw error;
    }
};