import { API_URL } from './config';
export const fetchAssignments = async (courseCode) => {
    if (!courseCode) {
        console.error('Course code is required');
        return [];
    }
    const apiKey = localStorage.getItem('apiKey');
    try {
        const response = await fetch(`${API_URL}/courses/${encodeURIComponent(courseCode)}/assignments`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${apiKey}`
            }
        });
        if (!response.ok) {
            throw new Error('Failed to fetch assignments');
        }
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error fetching assignments:', error);
        throw error;
    }
};
