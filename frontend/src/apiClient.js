import { API_URL } from './config';

// Maximum number of retries for any API request
const MAX_RETRIES = 3;
// Initial delay in milliseconds before retrying
const INITIAL_RETRY_DELAY = 1000;

export async function fetchWithRetry(url, options, retries = 0) {
    try {
        const response = await fetch(url, options);
        
        if (!response.ok) {
            // If unauthorized, clear credentials immediately and don't retry
            if (response.status === 401) {
                localStorage.removeItem('apiKey');
                localStorage.removeItem('sessionToken');
                throw new Error('unauthorized');
            }
            
            throw new Error(`Request failed with status ${response.status}`);
        }
        
        return await response.json();
    } catch (error) {
        // If we've exhausted our retries or it's an authentication error, throw
        if (retries >= MAX_RETRIES || error.message === 'unauthorized') {
            throw error;
        }
        
        // Wait with exponential backoff before retrying
        const delay = INITIAL_RETRY_DELAY * Math.pow(2, retries);
        console.log(`Retrying request (${retries + 1}/${MAX_RETRIES}) after ${delay}ms`);
        
        await new Promise(resolve => setTimeout(resolve, delay));
        return fetchWithRetry(url, options, retries + 1);
    }
}

export async function fetchCourses() {
    const apiKey = localStorage.getItem('apiKey');
    
    if (!apiKey) {
        throw new Error('No API key available');
    }
    
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${apiKey}`
    };

    return fetchWithRetry(`${API_URL}/courses/`, {
        method: 'GET',
        headers: headers
    });
}

export async function fetchAssignments(courseCode) {
    const apiKey = localStorage.getItem('apiKey');
    
    if (!apiKey) {
        throw new Error('No API key available');
    }
    
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${apiKey}`
    };

    return fetchWithRetry(`${API_URL}/courses/${encodeURIComponent(courseCode)}/assignments`, {
        method: 'GET',
        headers: headers
    });
}