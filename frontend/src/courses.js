import { API_URL } from './config';

export async function fetchCourses() {
    const apiKey = localStorage.getItem('apiKey');
    
    if (!apiKey) {
        // If no API key is found, throw a specific error
        throw new Error('No API key available');
    }
    
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${apiKey}`
    };

    try {
        const response = await fetch(`${API_URL}/courses/`, {
            method: 'GET',
            headers: headers
        });

        if (!response.ok) {
            // Handle authentication errors specifically
            if (response.status === 401) {
                localStorage.removeItem('apiKey'); // Clear the invalid API key
                localStorage.removeItem('sessionToken');
                throw new Error('unauthorized');
            }
            
            throw new Error(`Network response was not ok: ${response.status} ${response.statusText}`);
        }
        
        const courses = await response.json();
        return courses;
    } catch (error) {
        console.error('Error fetching courses:', error);
        throw error; // Re-throw to allow handling in the component
    }
};