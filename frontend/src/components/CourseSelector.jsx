import React, { useEffect, useState } from 'react';
import { fetchCourses } from '../apiClient'; // Update this import path as needed
import { useDebounce } from '../hooks/hooks'; // Update this import path as needed
import styles from '../css/Selector.module.css';

const CourseSelector = ({ onSelectCourse }) => {
    const [courses, setCourses] = useState([]);
    const [filteredCourses, setFilteredCourses] = useState([]);
    const [selectedYear, setSelectedYear] = useState('all'); // Default to 'all'
    const [availableYears, setAvailableYears] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [searchTerm, setSearchTerm] = useState('');
    const [sortOrder, setSortOrder] = useState('asc');
    const debouncedSearchTerm = useDebounce(searchTerm, 300);

    // Load courses on mount
    useEffect(() => {
        const loadCourses = async () => {
            try {
                setLoading(true);
                const data = await fetchCourses();
                setCourses(data);
                
                // Extract unique years from courses
                const years = [...new Set(data.map(course => course.year))]
                    .filter(year => year) // Filter out undefined/null years
                    .sort((a, b) => b - a); // Sort descending
                
                setAvailableYears(years);
                setError(null);
                
                // Set default filtered courses to be all courses initially
                setFilteredCourses(data);
            } catch (error) {
                console.error('Error loading courses:', error);
                
                if (error.message === 'unauthorized' || error.message === 'No API key available') {
                    // Redirect to login
                    window.location.href = '/login';
                    return;
                }
                
                setError('Failed to load courses. Please try again.');
            } finally {
                setLoading(false);
            }
        };
        
        loadCourses();
    }, []);

    // Handle filtering based on year, search term, and sort order
    useEffect(() => {
        if (!courses.length) return;

        let filtered = [...courses];

        // Apply year filter if not 'all'
        if (selectedYear !== 'all') {
            filtered = filtered.filter(course => course.year === parseInt(selectedYear));
        }

        // Apply search filter
        if (debouncedSearchTerm) {
            const searchLower = debouncedSearchTerm.toLowerCase();
            filtered = filtered.filter(course => 
                (course.name?.toLowerCase().includes(searchLower) ||
                 course.course_code?.toLowerCase().includes(searchLower))
            );
        }

        // Apply sorting
        filtered.sort((a, b) => {
            const nameA = formatCourseName(a).toLowerCase();
            const nameB = formatCourseName(b).toLowerCase();
            return sortOrder === 'asc' ? 
                nameA.localeCompare(nameB) : 
                nameB.localeCompare(nameA);
        });

        setFilteredCourses(filtered);
    }, [debouncedSearchTerm, selectedYear, sortOrder, courses]);

    const formatCourseName = (course) => {
        let formattedCode = '';
        if (course.course_code) {
            formattedCode = course.course_code.replace(/[\[\]]/g, '').trim();
            formattedCode = `${formattedCode} `;
        }
        return `${formattedCode}${course.name}`;
    };

    const formatAcademicYear = (year) => {
        if (!year) return 'Unknown';
        return `${year}/${year + 1}`;
    };

    const handleYearChange = (e) => {
        setSelectedYear(e.target.value);
    };

    const handleCourseSelect = (courseId) => {
        if (!courseId) return;
        onSelectCourse(courseId);
    };

    const toggleSortOrder = () => {
        setSortOrder(prev => prev === 'asc' ? 'desc' : 'asc');
    };
    
    if (loading && !courses.length) {
        return <div className={styles.loading}>Loading courses...</div>;
    }

    if (error) {
        return <div className={styles.error}>{error}</div>;
    }
    
    return (
        <div className={styles.selectorContainer}>
            <div className={styles.yearFilter}>
                <select 
                    className={styles.yearSelect}
                    value={selectedYear}
                    onChange={handleYearChange}
                >
                    <option value="all">All years</option>
                    {availableYears.map(year => (
                        <option key={year} value={year}>
                            {formatAcademicYear(year)}
                        </option>
                    ))}
                </select>
            </div>
            
            <div className={styles.searchAndSortContainer}>
                <input
                    type="text"
                    placeholder="Search courses..."
                    className={styles.searchInput}
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                />
                <button 
                    className={styles.sortButton}
                    onClick={toggleSortOrder}
                    title={`Sort ${sortOrder === 'asc' ? 'A to Z' : 'Z to A'}`}
                >
                    {sortOrder === 'asc' ? '↓' : '↑'}
                </button>
            </div>

            <div className={styles.courseSelectWrapper}>
                <select 
                    className={styles.courseSelect} 
                    onChange={e => handleCourseSelect(e.target.value)}
                    size={10}
                >
                    <option value="">Select a course</option>
                    {filteredCourses.map(course => (
                        <option key={course.id} value={course.id}>
                            {formatCourseName(course)} ({formatAcademicYear(course.year)})
                        </option>
                    ))}
                </select>
            </div>
        </div>
    );
};

export default CourseSelector;