import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import CourseSelector from '../components/CourseSelector';
import AssignmentSelector from '../components/AssignmentSelector';
import ChartComponent from '../components/ChartComponent';
import AssignmentList from '../components/AssignmentList';
import { fetchAssignments } from '../apiClient';
import styles from '../css/Home.module.css';

const Home = () => {
  const [selectedCourse, setSelectedCourse] = useState('');
  const [selectedAssignment, setSelectedAssignment] = useState('');
  const [liveAssignments, setLiveAssignments] = useState([]);
  const [recentAssignments, setRecentAssignments] = useState([]);
  const [isDemoMode, setIsDemoMode] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  // Initial session check
  useEffect(() => {
    const checkSession = () => {
      const demoMode = localStorage.getItem('demoMode') === 'true';
      setIsDemoMode(demoMode);
      
      if (demoMode) {
        setIsLoading(false);
        return;
      }
      
      const apiKey = localStorage.getItem('apiKey');
      if (!apiKey) {
        // Clear any stale data
        handleLogout();
        return;
      }
      
      setIsLoading(false);
    };

    checkSession();
    
    // Set up periodic checks but with a reasonable interval (every 5 minutes)
    const sessionCheck = setInterval(checkSession, 5 * 60 * 1000);
    
    return () => clearInterval(sessionCheck);
  }, []);

  const handleLogout = () => {
    localStorage.removeItem('apiKey');
    localStorage.removeItem('sessionToken');
    localStorage.removeItem('userData');
    localStorage.removeItem('courses');
    
    // Use navigate instead of direct modification when possible
    navigate('/login');
  };

  const handleSelectCourse = async (courseCode) => {
    if (!courseCode) return;
    
    try {
      setSelectedCourse(courseCode);
      setSelectedAssignment('');

      // Don't make API calls in demo mode
      if (isDemoMode) {
        const demoAssignments = [
          { id: 'demo1', name: 'Demo Assignment 1', end_at: new Date(2025, 2, 15).toISOString(), updated_at: new Date().toISOString() },
          { id: 'demo2', name: 'Demo Assignment 2', end_at: new Date(2025, 3, 1).toISOString(), updated_at: new Date().toISOString() }
        ];
        setLiveAssignments(demoAssignments);
        setRecentAssignments(demoAssignments);
        return;
      }

      const data = await fetchAssignments(courseCode);
      
      // If we get an empty response or error, don't proceed
      if (!data || !Array.isArray(data)) {
        console.error('Invalid assignment data received:', data);
        return;
      }

      const now = new Date();
      const live = data.filter(a => new Date(a.end_at) > now);
      const recent = [...data].sort((a, b) => new Date(b.updated_at) - new Date(a.updated_at));

      setLiveAssignments(live.slice(0, 5));
      setRecentAssignments(recent.slice(0, 5));
    } catch (error) {
      console.error('Error fetching assignments:', error);
      
      // Check if the error is authentication-related
      if (error.message?.includes('unauthorized') || 
          error.message?.includes('401') || 
          error.response?.status === 401) {
        handleLogout();
      }
    }
  };

  const handleSelectAssignment = (assignmentId) => {
    setSelectedAssignment(assignmentId);
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className={styles.appContainer}>
      <div className={styles.content}>
        <div id="side-menu" className={styles.sideMenu}>
          <h1>Welcome, <i>user</i>!</h1>
          {!isDemoMode && (
            <div className={styles.selectorsWrapper}>
              <h3>Select a course</h3>
              <CourseSelector onSelectCourse={handleSelectCourse} />
              
              {selectedCourse && (
                <div className={styles.assignmentSelectorContainer}>
                  <h3>Select an assignment</h3>
                  <AssignmentSelector 
                    courseCode={selectedCourse} 
                    onSelectAssignment={handleSelectAssignment} 
                  />
                </div>
              )}
            </div>
          )}
        </div>
        {isDemoMode ? (
          <div className={styles.chartContainer}>
            <ChartComponent courseCode="demoCourse" assignmentId="demoAssignment" isDemoMode={true} />
          </div>
        ) : (
          selectedAssignment ? (
            <div className={styles.chartContainer}>
              <ChartComponent courseCode={selectedCourse} assignmentId={selectedAssignment} />
            </div>
          ) : (
            <div className={styles.chartContainer}>
              <div className={styles.assignmentsContainer}>
                <AssignmentList 
                  title="Live assignments" 
                  assignments={liveAssignments} 
                  onClickAssignment={handleSelectAssignment} 
                />
                <AssignmentList 
                  title="Recently updated assignments" 
                  assignments={recentAssignments} 
                  onClickAssignment={handleSelectAssignment} 
                />
              </div>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Home;