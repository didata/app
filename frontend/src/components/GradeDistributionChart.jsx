import React from 'react';
import styles from '../css/GradeDistributionChart.module.css';

const GradeDistributionChart = ({ gradeDistribution }) => {
  if (!gradeDistribution || Object.keys(gradeDistribution).length === 0) {
    return null;
  }

  // Define the grade ranges in order
  const gradeRanges = [
    '0-1', '1-2', '2-3', '3-4', '4-5', 
    '5-6', '6-7', '7-8', '8-9', '9-10'
  ];

  // Calculate the maximum count for scaling
  const maxCount = Math.max(...Object.values(gradeDistribution));
  
  // Calculate the total count
  const totalCount = Object.values(gradeDistribution).reduce((sum, count) => sum + count, 0);
  
  // Helper function to get color based on grade range
  const getBarColor = (range) => {
    const [min] = range.split('-').map(parseFloat);
    if (min >= 5.75) {
      return '#4CAF50'; // Green for passing
    } else if (min >= 4) {
      return '#FFC107'; // Yellow for close
    } else {
      return '#F44336'; // Red for failing
    }
  };

  // Helper function to calculate width percentage
  const getWidthPercentage = (count) => {
    return maxCount > 0 ? (count / maxCount) * 100 : 0;
  };

  return (
    <div className={styles.gradeDistributionContainer}>
      <h3 className={styles.chartTitle}>Grade Distribution</h3>
      <div className={styles.gradeDistributionChart}>
        {gradeRanges.map(range => {
          const count = gradeDistribution[range] || 0;
          const percentage = totalCount > 0 ? (count / totalCount * 100).toFixed(1) : 0;
          
          return (
            <div key={range} className={styles.gradeBar}>
              <div className={styles.gradeLabel}>{range}</div>
              <div className={styles.barContainer}>
                <div 
                  className={styles.barFill}
                  style={{ 
                    width: `${getWidthPercentage(count)}%`,
                    backgroundColor: getBarColor(range)
                  }}
                >
                  {count > 0 && <span className={styles.barCount}>{count}</span>}
                </div>
              </div>
              <div className={styles.percentageLabel}>{percentage}%</div>
            </div>
          );
        })}
      </div>
      
      <div className={styles.distributionLegend}>
        <div className={styles.legendItem}>
          <div className={styles.legendColor} style={{ backgroundColor: '#F44336' }}></div>
          <span>Fail (0-4)</span>
        </div>
        <div className={styles.legendItem}>
          <div className={styles.legendColor} style={{ backgroundColor: '#FFC107' }}></div>
          <span>Almost Pass (4-5.75)</span>
        </div>
        <div className={styles.legendItem}>
          <div className={styles.legendColor} style={{ backgroundColor: '#4CAF50' }}></div>
          <span>Pass (5.75-10)</span>
        </div>
      </div>
    </div>
  );
};

export default GradeDistributionChart;