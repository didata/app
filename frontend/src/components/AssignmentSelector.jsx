import React, { useEffect, useState } from 'react';
import { fetchAssignments } from '../apiClient'; // Update import path as needed
import styles from '../css/Selector.module.css';

const AssignmentSelector = ({ courseCode, onSelectAssignment }) => {
    const [assignments, setAssignments] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    
    useEffect(() => {
        if (courseCode) {
            const loadAssignments = async () => {
                try {
                    setLoading(true);
                    const data = await fetchAssignments(courseCode);
                    console.log('AssignmentSelector loaded assignments:', data.length);
                    setAssignments(data);
                    setError(null);
                } catch (error) {
                    console.error('Error loading assignments:', error);
                    setError('Failed to load assignments');
                    setAssignments([]);
                    
                    // Handle authentication errors
                    if (error.message === 'unauthorized' || error.message === 'No API key available') {
                        window.location.href = '/login';
                    }
                } finally {
                    setLoading(false);
                }
            };
            loadAssignments();
        } else {
            setAssignments([]);
        }
    }, [courseCode]);

    if (loading) {
        return <div className={styles.loading}>Loading assignments...</div>;
    }

    if (error) {
        return <div className={styles.error}>{error}</div>;
    }

    return (
        <select 
            className={styles.assignmentSelect}
            onChange={e => onSelectAssignment(e.target.value)}
            defaultValue=""
        >
            <option value="" disabled>Select an assignment</option>
            {assignments.length === 0 ? (
                <option disabled>No assignments available</option>
            ) : (
                assignments.map(assignment => (
                    <option key={assignment.id} value={assignment.id}>
                        {assignment.name}
                    </option>
                ))
            )}
        </select>
    );
};

export default AssignmentSelector;