import React, { useEffect, useState } from 'react';
import axios from 'axios';

interface APIStats {
  today_stats: Record<string, number>;
  specific_course_requests: number;
  total_requests_today: number;
}

const APIStats: React.FC = () => {
  const [stats, setStats] = useState<APIStats | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchStats = async () => {
      try {
        const response = await axios.get('/api/stats/');
        setStats(response.data as APIStats);
      } catch (error) {
        console.error('Failed to fetch API stats:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchStats();
    // Refresh every 5 minutes
    const interval = setInterval(fetchStats, 5 * 60 * 1000);
    return () => clearInterval(interval);
  }, []);

  if (loading) return <div>Loading stats...</div>;
  if (!stats) return <div>No stats available</div>;

  return (
    <div className="api-stats">
      <h2>API Usage Statistics</h2>
      <div className="stats-card">
        <h3>Today's Usage</h3>
        <p>Total Requests: {stats.total_requests_today}</p>
        <p>Specific Course Requests: {stats.specific_course_requests}</p>
      </div>
      <div className="stats-breakdown">
        <h3>Endpoint Breakdown</h3>
        {Object.entries(stats.today_stats).map(([endpoint, count]) => (
          <div key={endpoint} className="endpoint-stat">
            <span>{endpoint}</span>
            <span>{count} requests</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default APIStats; 