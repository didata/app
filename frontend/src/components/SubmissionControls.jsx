import React from 'react';
import styles from '../css/ChartComponent.module.css'; // Make sure to update the path

const SubmissionControls = ({ viewMode, setViewMode, multipleSubmissionStats }) => {
    return (
        <div className={styles.submissionControls}>
            <div className={styles.viewModeContainer}>
                <label className={styles.modeLabel}>View Mode:</label>
                <div className={styles.buttonGroup}>
                    <button
                        className={`${styles.modeButton} ${viewMode === 'all' ? styles.activeMode : ''}`}
                        onClick={() => setViewMode('all')}
                    >
                        <span className={styles.radioCircle}></span>
                        All Submissions
                    </button>
                    <button
                        className={`${styles.modeButton} ${viewMode === 'highest' ? styles.activeMode : ''}`}
                        onClick={() => setViewMode('highest')}
                    >
                        <span className={styles.radioCircle}></span>
                        Highest Per Student
                    </button>
                </div>
            </div>
            
            {/* Show submission statistics when available */}
            {multipleSubmissionStats && (
                <div className={styles.statsContainer}>
                    <div className={styles.statsHeader}>Submission Statistics</div>
                    
                    {/* Show in-progress info if available */}
                    {multipleSubmissionStats.in_progress_count !== undefined && (
                        <div className={styles.statsItem}>
                            <span>In-progress assignments:</span>
                            <span className={styles.statsValue}>{multipleSubmissionStats.in_progress_count}</span>
                        </div>
                    )}
                    
                    {/* Show submitted info if available */}
                    {multipleSubmissionStats.submitted_count !== undefined && (
                        <div className={styles.statsItem}>
                            <span>Submitted assignments:</span>
                            <span className={styles.statsValue}>{multipleSubmissionStats.submitted_count}</span>
                        </div>
                    )}
                    
                    {/* Show multiple submissions info when in "all" view mode */}
                    {viewMode === 'all' && multipleSubmissionStats.percentage_multiple !== undefined && (
                        <>
                            <div className={styles.statsItem}>
                                <span>Students with multiple submissions:</span>
                                <span className={styles.statsValue}>{multipleSubmissionStats.percentage_multiple}%</span>
                            </div>
                            <div className={styles.statsItem}>
                                <span>Average submissions per student:</span>
                                <span className={styles.statsValue}>{multipleSubmissionStats.average_submissions}</span>
                            </div>
                        </>
                    )}
                </div>
            )}
        </div>
    );
};

export default SubmissionControls;