import React from 'react';
import styles from '../css/AssignmentStatsSummary.module.css';

const AssignmentStatsSummary = ({ inProgressCount, submittedCount, chartData }) => {
    if (!chartData || !chartData.rawGrades) return null;
    
    const passCount = chartData.rawGrades.Pass?.length || 0;
    const failCount = chartData.rawGrades.Fail?.length || 0;
    const totalCount = inProgressCount + submittedCount;
    const passRate = submittedCount > 0 ? (passCount / submittedCount * 100).toFixed(1) : 0;
    
    return (
      <div className={styles.statsContainer}>
        <div className={styles.statBox}>
          <p className={styles.statLabel}>In Progress</p>
          <p className={styles.statValueHighlight}>{inProgressCount}</p>
        </div>
        <div className={styles.statBox}>
          <p className={styles.statLabel}>Submitted</p>
          <p className={styles.statValueHighlight}>{submittedCount}</p>
        </div>
        <div className={styles.statBox}>
          <p className={styles.statLabel}>Total</p>
          <p className={styles.statValueHighlight}>{totalCount}</p>
        </div>
        <div className={styles.statBox}>
          <p className={styles.statLabel}>Passing</p>
          <p className={styles.statValueHighlight}>{passCount}</p>
        </div>
        <div className={styles.statBox}>
          <p className={styles.statLabel}>Failing</p>
          <p className={styles.statValueHighlight}>{failCount}</p>
        </div>
        <div className={styles.statBox}>
          <p className={styles.statLabel}>Pass Rate</p>
          <p className={styles.statValueHighlight}>{passRate}%</p>
        </div>
      </div>
    );
  };

export default AssignmentStatsSummary;