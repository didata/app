import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styles from '../css/RequestCount.module.css';

const RequestCount = () => {
  const [error, setError] = useState(null);

  const generateTimeSlots = () => {
    const slots = {};
    for (let hour = 0; hour < 24; hour++) {
      for (let minute = 0; minute < 60; minute++) {
        const timeString = `${hour.toString().padStart(2, '0')}:${minute.toString().padStart(2, '0')}`;
        slots[timeString] = 0;
      }
    }
    return slots;
  };

  const [stats, setStats] = useState({
    totalToday: 0,
    perMinute: generateTimeSlots()
  });

  const fetchData = async () => {
    try {
      const response = await axios.get('/api/request-count/');
      setStats({
        totalToday: response.data.total_today,
        perMinute: { ...generateTimeSlots(), ...response.data.per_minute }
      });
      setError(null);
    } catch (err) {
      console.error('Error fetching request count:', err);
      setError('Failed to load request statistics');
    }
  };

  useEffect(() => {
    fetchData();
    const interval = setInterval(fetchData, 30000); // Update every 30 seconds
    
    return () => clearInterval(interval); // Cleanup on unmount
  }, []);

  if (error) {
    return <div className={styles.errorMessage}>{error}</div>;
  }

  return (
    <div className={styles.requestCountContainer}>
      <h2>API Request Statistics</h2>
      <div className={styles.statsContainer}>
        <div className={styles.totalRequests}>
          <h3>Total Requests Today</h3>
          <div className={styles.count}>{stats.totalToday}</div>
        </div>
        
        <div className={styles.minuteRequests}>
          <h3>Requests per Minute</h3>
          <div className={styles.tableWrapper}>
            <table>
              <thead>
                <tr>
                  <th>Time</th>
                  <th>Count</th>
                </tr>
              </thead>
              <tbody>
                {Object.entries(stats.perMinute)
                  .sort()
                  .map(([time, count]) => (
                    <tr key={time} className={count > 0 ? styles.hasRequests : ''}>
                      <td>{time}</td>
                      <td>{count}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RequestCount; 