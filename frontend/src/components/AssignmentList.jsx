import React from 'react';
import styles from '../css/AssignmentList.module.css';

const AssignmentList = ({ title, assignments, onClickAssignment, loading, error }) => {
  const isAssignmentLive = (assignment) => {
    const endDate = new Date(assignment.end_at);
    return endDate > new Date();
  };

  if (loading) {
    return (
      <div className={styles.assignmentList}>
        <h3>{title}</h3>
        <div className={styles.loading}>Loading assignments...</div>
      </div>
    );
  }

  if (error) {
    return (
      <div className={styles.assignmentList}>
        <h3>{title}</h3>
        <div className={styles.error}>{error}</div>
      </div>
    );
  }

  return (
    <div className={styles.assignmentList}>
      <h3>{title}</h3>
      {assignments && assignments.length > 0 ? (
        <ul>
          {assignments.map(assignment => (
            <li
              key={assignment.id}
              onClick={() => onClickAssignment?.(assignment.id)}
              className={styles.assignmentItem}
            >
              <span className={styles.assignmentName}>{assignment.name}</span>
              {isAssignmentLive(assignment) && (
                <span className={styles.liveIndicator}>
                  <span className={styles.liveDot}></span>
                  LIVE
                </span>
              )}
            </li>
          ))}
        </ul>
      ) : (
        <p className={styles.noAssignments}>No assignments to display.</p>
      )}
    </div>
  );
};

export default AssignmentList;