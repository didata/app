#!/bin/bash

# Set the project root directory
PROJECT_ROOT="/var/web_server/htdocs/didata"

# Navigate to the frontend directory
cd "$PROJECT_ROOT/frontend"

# Add missing dependency to package.json if not already present
if ! grep -q '"@babel/plugin-proposal-private-property-in-object"' package.json; then
  sudo npm install --save-dev @babel/plugin-proposal-private-property-in-object
fi

# Build the frontend
sudo npm run build

# Navigate back to the project root directory
cd "$PROJECT_ROOT"

# Bring down the Docker containers
sudo docker-compose down

# Rebuild the backend specifically
sudo docker-compose build backend

# Bring up the Docker containers with the new build
sudo docker-compose up -d

echo "Build and deployment completed successfully."
